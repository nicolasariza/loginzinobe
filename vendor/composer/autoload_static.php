<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitc3b44dec931e6b9206f3cdd5241dd96c
{
    public static $files = array (
        '3c3a02bc9945aa3571024986c07a1e55' => __DIR__ . '/..' . '/rinvex/countries/src/helpers.php',
    );

    public static $prefixLengthsPsr4 = array (
        'R' => 
        array (
            'Rinvex\\Country\\' => 15,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Rinvex\\Country\\' => 
        array (
            0 => __DIR__ . '/..' . '/rinvex/countries/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitc3b44dec931e6b9206f3cdd5241dd96c::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitc3b44dec931e6b9206f3cdd5241dd96c::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
