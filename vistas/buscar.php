<?php
if(!isset($_SESSION['documento']))
{
    header('location:index.php?controlador=usuario&accion=login');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/custom.css">
    <title>Zinobe buscar</title>
</head>

<body>
    <div class="container">
    <a href="index.php?controlador=usuario&accion=logout" type="button" class="btn btn-danger float-right mt-3">Logout</a>
        <div class="row justify-content-center align-items-center" style="height:50vh">
            <div class="col-12 col-md-10 col-lg-8">
                <form action="index.php?controlador=usuario&accion=obtenerUsuarios" method="post" class="card card-sm">
                    <div class="card-body row no-gutters align-items-center">
                        <div class="col-auto">
                            <i class="fas fa-search h4 text-body"></i>
                        </div>
                        <div class="col">
                            <input class="form-control form-control-lg" type="search" name="buscar" placeholder="ingresar email o nombre">
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-lg btn-success" type="submit">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php if(isset($busquedas)):?>
            <table class="table">
  <thead>
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Documento</th>
      <th scope="col">Email</th>
      <th scope="col">Pais</th>
    </tr>
  </thead>
  <tbody>
      <?php foreach($busquedas as $busqueda){
          echo "<tr><td>".$busqueda['nombre']."</td><td>".$busqueda['documento']."</td><td>".$busqueda['email']."</td><td>".$busqueda['pais']."</td></tr>";
      } 
      ?>
  </tbody>
</table>
        <?php endif;?>
    </div>
</body>

</html>