<?php
if(isset($_SESSION['documento']))
{
    header('location:index.php?controlador=usuario&accion=buscar');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/custom.css">
    <title>Zinobe login</title>
</head>
<body>
<div class="container">
			<?php if(isset($resultado)):?>
				<div class="alert alert-danger float mt-5" role="alert">
				<?=$resultado?>
				</div>
			<?php endif;?>
			<div class="row justify-content-center align-items-center" style="height:100vh">
				<div class="col-lg-5">		
					<div class="card">
                        <div class="container">
							<h2>Login</h2>
							<form action="index.php?controlador=usuario&accion=loginVerificacion" method="post">                           	
								<div class="form-group">									
									<input type="number" class="form-control input-lg" name="documento" placeholder="Documento" required>        
								</div>							
								<div class="form-group">        
									<input type="password" class="form-control input-lg" name="password" placeholder="Password" required>       
								</div>								    
									<button type="submit" class="btn btn-success btn-block">Login</button><br>
									<a href="index.php?controlador=usuario&accion=registro" type="button">Registrarse</a>
							</form>
							<br>
                            </div>
					</div>
				</div>
			</div>
		</div>
</body>
</html>
