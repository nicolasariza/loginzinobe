<?php
    require 'vendor/autoload.php';
if(isset($_SESSION['documento']))
{
    header('location:index.php?controlador=usuario&accion=buscar');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/custom.css">
<title>Zinobe buscar</title>
</head>

<body>
    <div class="container">
		<a href="index.php?controlador=usuario&accion=login" type="button" class="btn btn-primary float-right mt-3">Regresar</a>
        <div class="row justify-content-center align-items-center" style="height:100vh">
            <div class="col-lg-5">
                <div class="card">
                    <div class="container">
                        <h2>Registro</h2>
                        <form action="index.php?controlador=usuario&accion=guardar" method="post">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control input-lg" minlength="3" name="nombre" placeholder="Nombre" id="nombre" required>
                            </div>
                            <div class="form-group">
                                <label for="documento">Identificación</label>
                                <input type="number" class="form-control input-lg" name="documento" placeholder="Documento" id="documento" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control input-lg" name="email" placeholder="Email" id="email" required>
                            </div>
                            <div class="form-group">
                                <label for="pais">País</label>
                                <select class="form-control" name="pais" id="pais">
                                    <?php
                                    $countries = countries();
                                    foreach ($countries as $countrie) {
                                        echo '<option value="'.$countrie['name'].'">' . $countrie['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control input-lg" minlength="6" name="password" placeholder="Password" id="password" required>
                            </div>
                            <button type="submit" class="btn btn-success btn-block">Registrarse</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>