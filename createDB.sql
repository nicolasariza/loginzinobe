CREATE DATABASE zinobe;
USE zinobe;

CREATE TABLE usuarios(
    id int(255) PRIMARY KEY AUTO_INCREMENT NOT NULL,
    nombre varchar(100) not null,
    documento varchar(50) not null,
    email varchar(100) not null,
    pais varchar(50) not null,
    password varchar(255) not null,
    unique(documento),
    unique(email)
);