# loginZinobe
##### Se realiza login usando Programación orientada a objetos y MVC, se hace uso de la librería rinvex/countries

###### Instalación 
1. Contar con wampp o xampp, ejecutar mysql y apache
2. ubicar proyecto en la carpeta htdocs o www dependiendo de la herramienta que se instale
3. ejecutar el archivo createDB.sql ubicado en la raiz del proyecto **revisar el nombre de la base de datos**

###### Instrucciones de uso
1. En el navegador ingresar la siguiente ruta: http://localhost/loginzinobe/
2. Dar click en el enlace 'Registrarse'
3. Llenar los campos y registrarse
4. Al registrarse se redirecciona a buscar en donde se ingresa el email o nombre para encontrar resultados
5. Para cerrar sesión dar click en 'logout'
6. Esto redireccionará al login de nuevo, alli se podrá loguear con las credenciales ingresadas

###### Notas
1. Mientras se está logueado no se podrá ir al login o a la pagina de registro
2. Se muestran alertas si al momento del login el usuario no existe o dijita mal la password
3. Usuario no logueados no podrán buscar
4. se hace uso de composer para librería de paises 

#2023
testing for amwl repository 
