<?php
require 'modelos/CustomerData.php';
class usuarioControlador{
    
    public function login(){
        include 'vistas/login.php';
    }

    public function registro(){
        include 'vistas/registro.php';
    }

    public function buscar(){
        include 'vistas/buscar.php';
    }
    public function guardar(){
        if(isset($_POST)){
            $usuario = new CustomerData();
            $usuario->nombre=$_POST['nombre'];
            $usuario->documento=$_POST['documento'];
            $usuario->email=$_POST['email'];
            $usuario->pais=$_POST['pais'];
            $usuario->password=password_hash($_POST['password'],PASSWORD_DEFAULT);
            $resultado = $usuario->guardar();
            if($resultado){
                $_SESSION['documento']=$usuario->documento;
                header("location:index.php?controlador=usuario&accion=buscar");
            }else{
                header("location:index.php?controlador=usuario&accion=registro");
            }
        }
    }

    public function obtenerUsuarios(){
        if(isset($_POST)){
            $usuario = new CustomerData();
            $usuario->nombre=$_POST['buscar'];
            $usuario->email=$_POST['buscar'];
            $busquedas = $usuario->obtenerUsuarios();
            include('vistas/buscar.php');

        }
    }

    public function logout(){
        session_destroy();
        header("location:index.php");
    }

    public function loginVerificacion(){
        if(isset($_POST)){
            $usuario = new CustomerData();
            $documento = $_POST['documento'];
            $password = $_POST['password'];
            $usuario->documento=$documento;
            $usuario->password=$password;
            $resultado = $usuario->loginVerificacion();
            if($resultado == 'NO'){
                $resultado = 'Usuario no existe';
            }else{
                $resultado =password_verify($password,$resultado['password']);
                if($resultado){
                    $_SESSION['documento']=$usuario->documento;
                    header("location:index.php?controlador=usuario&accion=buscar");
                }
                else{
                    $resultado = 'Error password';
                }
            }
            include('vistas/login.php');
        }

    }
}
