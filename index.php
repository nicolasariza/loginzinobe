<?php
include 'autoload.php';
include 'configuracion/db.php';
session_start();
if(isset($_GET['controlador'])){
    $controlador_nombre = $_GET['controlador'].'Controlador';
}
else{
    header("location: index.php?controlador=usuario&accion=login");
    exit();
}

if(class_exists($controlador_nombre)){
    $controlador = new $controlador_nombre();
    if(isset($_GET['accion']) && method_exists($controlador,$_GET['accion']))
    {
        $accion = $_GET['accion'];
        $controlador->$accion();
    }
    else{
        echo "404";
    }
}
else{
    echo "404";
}
?>